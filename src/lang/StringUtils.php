<?php


namespace crack9527\utils\lang;


/**
 * Class StringUtils
 * @package crack9527\utils\lang
 */
class StringUtils
{
    /**
     * 判断字符串是否为空
     * @param string $value
     * @return bool true-空字符,false-非空
     */
    public static function isEmpty($value)
    {
        return '' === $value || null === $value || 0 == strlen($value);
    }
}