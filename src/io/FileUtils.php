<?php


namespace crack9527\utils\io;


/**
 * Class FileUtils
 * @package crack9527\utils\io
 */
class FileUtils
{
    /**
     * 删除目录
     * @param $directory
     * @return bool
     */
    public static function deleteDirectory($directory)
    {
        if (is_dir($directory)) {
            //在删除之前，把里面的文件全部删掉
            $dir = opendir($directory);
            while ($dirName = readdir($dir)) {
                //必须加这一项，不然可能会将整个磁盘给删掉
                if ($dirName != "." && $dirName != "..") {
                    $durl = $directory . DIRECTORY_SEPARATOR . $dirName;
                    if (is_file($durl)) {
                        unlink($durl);
                    } else {
                        static::deleteDirectory($durl);
                    }
                }
            }
            closedir($dir);
            //删除该文件夹
            rmdir($directory);
        } else {
            //如果是文件，直接删掉
            unlink($directory);
        }
        return true;
    }

    /**
     * 创建目录
     * @param string $directory
     * @param int $mode
     * @param bool $recursive
     * @return bool 成功时返回 TRUE, 或者在失败时返回 FALSE
     */
    public static function createDirectory($directory = '', $mode = 0777, $recursive = true)
    {
        return is_dir($directory) ? true : mkdir($directory, $mode, $recursive);
    }
}