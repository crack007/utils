<?php

namespace crack9527\utils\io;

use PHPUnit\Framework\TestCase;

class FileUtilsTest extends TestCase
{
    private $_testDirectory;

    protected function setUp()
    {
        $this->_testDirectory = __DIR__ . '/../../../tmp';
    }

    public function testDeleteDirectory()
    {
        $this->testCreateDirectory();
        $this->assertTrue(FileUtils::deleteDirectory($this->_testDirectory));
    }

    public function testCreateDirectory()
    {
        $this->assertTrue(FileUtils::createDirectory($this->_testDirectory));
    }
}
